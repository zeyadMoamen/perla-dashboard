export default {
    mode: 'universal',
    /*
     ** Headers of the page
     */
    head: {
        title: process.env.npm_package_name || '',
        meta: [{
                charset: 'utf-8'
            },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1'
            },
            {
                hid: 'description',
                name: 'description',
                content: process.env.npm_package_description || ''
            }
        ],
        link: [{
                rel: "icon",
                type: "image/x-icon",
                href: 'logo.jpg'

            },
            {
                rel: 'stylesheet',
                href: "https://fonts.googleapis.com/css2?family=Mada&display=swap"
            },
            {
                rel: 'stylesheet',
                href: "https://fonts.googleapis.com/css2?family=Metal+Mania&display=swap"
            },
            {
                rel: "stylesheet",
                href: "https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"
            }

        ]

    },
    /*
     ** Customize the progress-bar color
     */
    loading: {
        color: '#fff'
    },
    /*
     ** Global CSS
     */
    css: [
        'assets/all.min.css'
    ],
    /*
     ** Plugins to load before mounting the App
     */
    plugins: [{
            src: '@/plugins/vue-awesome-swiper',
            mode: 'client'
        },
        {
            src: '@/plugins/vuesax'
        },
        {
            src: '@/plugins/snotify.js'
        },
        {
            src: '@/plugins/v-select.js'
        },
        {
            src: '@/plugins/form-wizard.js'
        }

    ],
    /*
     ** Nuxt.js dev-modules
     */
    buildModules: [],
    /*
     ** Nuxt.js modules
     */
    modules: [
        // Doc: https://bootstrap-vue.js.org
        'bootstrap-vue/nuxt',
        // Doc: https://axios.nuxtjs.org/usage
        '@nuxtjs/axios',
        '@nuxtjs/auth'
    ],
    /*
     ** Axios module configuration
     ** See https://axios.nuxtjs.org/options
     */
    axios: {
        baseURL: "https://perla-be-2.herokuapp.com/api/v1/",
        common: {
            Accept: "application/json"
        }
      },
    /*
     ** Build configuration
     */
    build: {
        /*
         ** You can extend webpack config here
         */
        extend(config, ctx) {}
    },
    auth: {
        // Options
        strategies: {
            local: {
                endpoints: {
                    login: {
                        url: "login",
                        method: "post",
                        propertyName: "token"
                    },
                    user:false,
                    logout: false
                },
                // tokenRequired: true,
                tokenType: "Bearer"
            }
        }
    },
}